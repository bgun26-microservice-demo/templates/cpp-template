#include "gtest/gtest.h"
#include "main/hello-greet.h"

TEST(HelloTest, GetGreet) {
    auto val = get_greet("Bazel");
    auto expected = "Hello bazel";
    EXPECT_EQ(val, expected);
    // EXPECT_EQ(val, expected) << "Expected " << expected << " got " << val;
}